const { DateTime } = require("luxon");

module.exports = function(eleventyConfig) {
  // Nice date functions
  eleventyConfig.addFilter("readableDate", dateObj => {
    return DateTime.fromJSDate(dateObj, {zone: 'utc'}).toFormat("dd LLL yyyy");
  });

  // https://html.spec.whatwg.org/multipage/common-microsyntaxes.html#valid-date-string
  eleventyConfig.addFilter('htmlDateString', (dateObj) => {
    return DateTime.fromJSDate(dateObj, {zone: 'utc'}).toFormat('yyyy-LL-dd');
  });

  // Copy the 'style.css' file through
  eleventyConfig.addPassthroughCopy('style.css');

  // Markdown config
  let markdownIt = require("markdown-it");
  let markdownItAnchor = require("markdown-it-anchor");
  let markdownItFootnote = require("markdown-it-footnote");
  let markdownItOptions = {
    html: true,
    breaks: false,
    linkify: true
  }
  let markdownItAnchorOptions = {
    permalink: false,
    permalinkClass: "heading__anchorLink",
    permalinkSymbol: "#"
  }

  eleventyConfig.setLibrary("md", markdownIt(markdownItOptions)
    .use(markdownItAnchor, markdownItAnchorOptions)
    .use(markdownItFootnote)
  );

  return {
    templateFormats: [
      "md",
      "njk",
      "html",
      "liquid"
    ],
    markdownTemplateEngine: "liquid",
    htmlTemplateEngine: "njk",
    dataTemplateEngine: "njk",
    passthroughFileCopy: true,
    dir: {
      includes: "_includes",
      data: "_data",
      output: "public"
    }
  }
}
