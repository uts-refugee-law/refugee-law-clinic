[UTS Refugee Law Clinic website](https://uts-refugee-law.gitlab.io/refugee-law-clinic/)
========================================

See https://uts-refugee-law.gitlab.io/refugee-law-clinic/

This is the source code for the UTS Law, Refugee Law Clinic website.

The site contains supporting resources for the University of Technology Sydney, Faculty of Law's clinical Refugee Law and Practice subject.
The subject introduces students to the legal principles and procedures of international refugee law and their application within the Australian domestic context.
Participating students are exposed to the practical realities of refugee law and practice through real world experience, with supervision by UTS Law academics together with a local refugee legal organisation.

The site uses [Eleventy](https://www.11ty.io/), and [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/)
to compile and deploy the resource files into the website.

## Contributing and updating resources

See [the CONTRIBUTING.md file](https://gitlab.com/uts-refugee-law/refugee-law-clinic/blob/master/CONTRIBUTING.md).

## Configuration

See the `_data/metadata.json` file.

* `repositoryFilesUrl`, *explain here how to add an edit link to posts*.

## Development

### Dependencies

The posts and templates and compiled into the website using [Eleventy](https://www.11ty.io/docs/usage/), a node.js based static site generator.

Once you have Node installed, you can install all the dependencies for this
project by running `npm install` in the project directory.

### Compiling the site

The posts and templates and compiled into the website using [Eleventy](https://www.11ty.io/docs/usage/).

To build the site into the `public/` folder, run:

```
npx eleventy
```

Eleventy can watch for file changes and serve the site to a local development
server, with:

```
npx eleventy --watch
```

Run with debug output

```
DEBUG=Eleventy* npx eleventy --serve
```

## Deployment
