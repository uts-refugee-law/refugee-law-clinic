Hi there. It's great that you're interested in contributing to the [UTS Refugee Law Clinic website](https://uts-refugee-law.gitlab.io/refugee-law-clinic/).

Welcome contributions include:

* Adding another useful resource to the [Key Websites and Further Resources page](https://uts-refugee-law.gitlab.io/refugee-law-clinic/resources/further-resources/).
* Updating the other guides if some information has gone out of date, or adding links or other relevant information.

If you have other ideas, that's great too.
You might like to contact the clinic team (you can find their details in the site's footer), about your idea and get their feedback. 

You can view all [the files that are compiled to make this website](https://gitlab.com/uts-refugee-law/refugee-law-clinic/tree/master),
[view their history](https://gitlab.com/uts-refugee-law/refugee-law-clinic/commits/master/resources), and even suggest changes,
because it is [Open Source](https://en.wikipedia.org/wiki/Open-source_model)
and [publicly hosted on GitLab](https://gitlab.com/uts-refugee-law/refugee-law-clinic/).

The files for all the clinic resources listed on [the home page of the site](https://uts-refugee-law.gitlab.io/refugee-law-clinic/)
can be found in [the `/resources` folder](https://gitlab.com/uts-refugee-law/refugee-law-clinic/tree/master/resources)
in the [files repository](https://gitlab.com/uts-refugee-law/refugee-law-clinic/tree/master).

**First step for contributing is to [register for a free GitLab account](https://gitlab.com/users/sign_in#register-pane)**.

**All the resources are formatted in [Markdown](https://en.wikipedia.org/wiki/Markdown#Example), a simple text formatting pattern for the web**.

### Editing resources

You can suggest updates to any of the clinic resources, once [signed into your free GitLab account](https://gitlab.com/users/sign_in),
by editing the [files that they are compiled from in the `resources` folder](https://gitlab.com/uts-refugee-law/refugee-law-clinic/tree/master/resources).

1. Open the file for the clinic resource you want to contribute to,
   [the *Key Websites and Further Resources* entry](https://gitlab.com/uts-refugee-law/refugee-law-clinic/blob/master/resources/further-resources.md) for example,
   and find the 'Edit' button.
2. When you click 'Edit', GitLab will ask you to 'Fork' the project,
   so that you can make a 'merge request'—this is coding language that means,
   'make a copy of this project so that you can use it to suggest changes to the original'.
3. Once you click 'Fork' you'll be presented with the text of the file to edit.
   Now go ahead and make your changes to the text;
   underneath, write a 'Commit message' that communicates the purpose of your change;
   and finally, hit the 'Commit changes' button.
4. You'll now be at the form to make a 'New Merge Request' (this is the way programmers make contributions to open source software projects).
   Write anything additional you'd like to describe your suggested changes,
   and hit the 'Submit merge request' button at the bottom.
5. That's it, well done! The project administrators will get an email to let them now
   and they'll be able to review, provide feedback, and merge in your
   suggestions to the project.

If you're an administrator of the code repository, you can make changes directly and skip the merge request.

### Adding resources

Add new resources to the site by adding new files to [the `resources` folder](https://gitlab.com/uts-refugee-law/refugee-law-clinic/commits/master/resources),
by clicking the '+' button at the top of [the resources folder page](https://gitlab.com/uts-refugee-law/refugee-law-clinic/commits/master/resources)
in `refugee-law-clinic / resources / +`.

When adding new files, first open the one the exiting files and pay close attention to how it is named,
and the metadata at the top of the file ([YAML front matter](https://www.11ty.io/docs/data-frontmatter/)).
You'll have an easier time if you copy and paste as much as possible, then make your changes.

Here's the key points:

1. The name of your file should have '-' instead of spaces, and should end in `.md` (it's a [Markdown file](https://en.wikipedia.org/wiki/Markdown) after-all).
2. The file must begin with a section of metadata, starting and ending with three dashes (`---`).
   In this section, you declare the resources title, the summary
   the will appear on the home page, and the 'Updated at' date that will be displayed.
   It is strongly suggested that you copy and paste this from an existing resource and then edit it.
3. Use [Markdown formatting](https://en.wikipedia.org/wiki/Markdown#Example) for the headings, links, blockquotes, emphasis etc. .
   Compare the [Markdown in the source of a file in the code repository](https://gitlab.com/uts-refugee-law/refugee-law-clinic/blob/master/resources/ethical-lawyering-in-a-refugee-law-context.md#L8)
   with how they are [displayed on the site](https://uts-refugee-law.gitlab.io/refugee-law-clinic/resources/ethical-lawyering-in-a-refugee-law-context/) to understand how your resource will be rendered.
4. Follow the 'editing resources' steps above for writing a descriptive 'Commit message' and to make a 'Merge request'.

### Editing the home page and footer

You can also edit the templates for the [home page (index.njk)](https://gitlab.com/uts-refugee-law/refugee-law-clinic/blob/master/index.njk)
and the [site's footer (siteFooter.njk)](https://gitlab.com/uts-refugee-law/refugee-law-clinic/blob/master/_includes/siteFooter.njk).

Follow the resource editing steps above to make a merge request.
