---
postTitle: Key Websites and Further Resources
description:
  Key websites and resources for refugee law casework. 
date: 2019-03-27
---

This is a growing list of key resources for Refugee Law and Practice in Australia. 


## Refugee Status Determination (RSD) decisions in Australia

[Immigration Assessment Authority (IAA)](http://www.iaa.gov.au) including [IAA decisions](http://www.iaa.gov.au/about/decisions)

[Migration and Refugee Division](https://www.aat.gov.au/resources/fact-sheets/migration-and-refugee-review-fact-sheets), [Administrative Appeals Tribunal (AAT)](https://www.aat.gov.au/) including [AAT decisions](http://www.austlii.edu.au/cgi-bin/viewdb/au/cases/cth/AATA/)


## Key Resources on Refugee Law in Australia

AAT, [Guide to Refugee Law in Australia](https://www.aat.gov.au/guide-to-refugee-law-in-australia)

Analysis and Policy Observatory (APO), [Forced Migration](https://apo.org.au/taxonomy/term/2926)

Australian Human Rights Commission, [Asylum Seekers and Refugees](https://www.humanrights.gov.au/our-work/asylum-seekers-and-refugees)

[Human Rights Law Centre](https://www.hrlc.org.au/refugee-and-asylum-seeker-rights)

University of New South Wales, [Kaldor Centre for International Refugee Law](https://www.kaldorcentre.unsw.edu.au/)

University of Melbourne, [Refugee and Asylum Seeker Law Research Guide](http://unimelb.libguides.com/refugee_law)

University of Queensland, [Migration and Refugee Law Guide](http://guides.library.uq.edu.au/migration-and-refugee-law)

Parliament of Australia, [Asylum seekers and refugees: a quick guide to key Parliamentary Library publications](https://www.aph.gov.au/About_Parliament/Parliamentary_Departments/Parliamentary_Library/pubs/rp/rp1617/Quick_Guides/AsylumRefugee) (2016)

State Library NSW, [Hot Topics 77: Refugees](https://legalanswers.sl.nsw.gov.au/hot-topics-77-refugees)

Library of Congress, [Refugee Law and Policy: Australia](https://www.loc.gov/law/help/refugee-law/australia.php) (2016)


## Key Resources on International Refugee Law

[RefWorld](https://www.refworld.org/)

UNHCR, [Guide to International Refugee Protection and Building State Asylum Systems](https://www.unhcr.org/publications/legal/3d4aba564/refugee-protection-guide-international-refugee-law-handbook-parliamentarians.html) (2017)

UNHCR, [Global Consultations](https://www.unhcr.org/en-au/protection/globalconsult/3b95cbce4/global-consultations-general.html) (2001)

[Refugee Law Reader](https://www.refugeelawreader.org/en/)

[University of Michigan's RefLaw Project](http://www.reflaw.org/)

Oxford University Press, [Resources on Refugee Law](http://opil.ouplaw.com/page/refugee-law)


## Stay Informed

[Asylum Insight](https://www.asyluminsight.com/)

[International Detention Coalition](https://idcoalition.org/)

[openMigration](https://www.opendemocracy.net/en/openmigration/)

[Refugee Council of Australia (RCOA)](https://www.refugeecouncil.org.au/)

[Refugees Deeply](https://www.newsdeeply.com/refugees)

[Road to Refuge](http://www.roads-to-refuge.com.au/index.html)


## Refugee Advocacy and Legal Service Providers in Australia

[Australia Pro Bono Directory](http://www.refugeelegalaidinformation.org/australia-pro-bono-directory)

[Australian Migrant Resource Centre](https://amrc.org.au/) (SA)

[Australian Refugee Action Network (RAN)](https://australianrefugeeactionnetwork.wordpress.com/)

[Asylum Seeker Resource Centre (ASRC)](https://www.asrc.org.au/human-rights-law/) (Vic)

[Centre for Asylum Seekers, Refugees and Detainees (CARAD)](https://amrc.org.au/) (WA)

[Darwin Asylum Seeker Support and Advocacy Network (DASSAN)](https://www.dassan.org/)

[Immigration Advice & Rights Centre (IARC)](https://iarc.asn.au/) (NSW)

[National Justice Project](https://justice.org.au/) (NSW)

[Refugee Action Coalition (RAC)](http://www.refugeeaction.org.au/) (NSW)

[Refugee Advice and Casework Service (RACS)](https://www.racs.org.au/causes/protection-visa-assistance/) (NSW)

[Refugee Advocacy Service of South Australia (RASSA)](https://rassa.org.au/) (SA)

[Refugee and Immigration and Legal Service (RAILS)](http://www.rails.org.au/) (Qld)

[Refugee Legal](https://refugeelegal.org.au/) (formerly RILC) (Vic)

[Salvos Legal Humanitarian](https://www.salvoslegal.com.au/) (NSW)

[The Humanitarian Group](https://thehumanitariangroup.org.au/) (WA)


## Additional Resources

### Films about Australia's Refugee Law and Policy

[Between the Devil and the Deep Blue Sea](https://www.imdb.com/title/tt0112491/) (2011, dir. David Schmidt)

[Chasing Asylum](https://www.imdb.com/title/tt5134588/) (2016, dir. Eva Orner)

[Chauka, Please Tell Us the Time](https://www.imdb.com/title/tt6229152/) (2017, dir. Behrouz Boochani & Arash Kamali Sarvestani)

[Letters to Ali](https://www.imdb.com/title/tt0424235/?ref_=fn_al_tt_1) (2004, dir. Clara Law)

[Mary meets Mohammad](https://www.imdb.com/title/tt3756454/) (2013, dir. Heather Kirkpatrick)

[We Will Be Remembered for This](https://www.imdb.com/title/tt4151630/?ref_=nm_knf_t2) (2007, dir. David Schmidt)



