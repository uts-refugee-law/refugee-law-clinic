---
postTitle: Ethical Lawyering in a Refugee Law Context
description:
  When working in a clinic, students are bound by a number of important duties and rules. Working with asylum seekers also raises a number of ethical responsibilities. This document outlines what is required of students working in the UTS Refugee Clinic, as well as the rules that govern the ethical and professional duties of admitted solicitors in NSW.
date: 2019-03-07
---

## Contents

* [Overview](#overview)
* [A. Ethical Lawyering in Practice: Key Issues in RSD](#a.-ethical-lawyering-in-practice%3A-key-issues-in-rsd)
  * [(i) Client Confidentiality](#(i)-client-confidentiality)
  * [(ii) Working with Asylum Seeker and Refugee Clients](#(ii)-working-with-asylum-seeker-and-refugee-clients)
  * [(iii) Working with Interpreters in Refugee Casework](#(iii)-working-with-interpreters-in-refugee-casework)
* [B. Legal Duties of Lawyers working with Refugee and Asylum Seeker Clients](#b.-legal-duties-of-lawyers-working-with-refugee-and-asylum-seeker-clients)
  * [(i) NSW Legislation](#(i)-nsw-legislation)
  * [(ii) Commonwealth Legislation](#(ii)-commonwealth-legislation)
* [C. The Importance of Legal Assistance in RSD](#c.-the-importance-of-legal-assistance-in-rsd)
  * [Further resources](#further-resources-3)

## Overview

Working as a lawyer with asylum seeker and refugee clients can entail great ethical responsibilities and challenges. In Australia and elsewhere, lawyers with asylum seeker and refugee clients work in intensely politicised environments, often with limited resources, changing political and legal frameworks, and clients who have had traumatic experiences and who may be undergoing prolonged periods of uncertainty. The refugee status determination (RSD) process is often an incredibly stressful and at times re-traumatising process for asylum seekers. Lawyers working with refugee and asylum seeker clients are both a part of this refugee status determination process as well as an advocate for their client’s interest within this process. 

When interpreting and discharging their professional obligations and conducting themselves in an ethical matter, lawyers working with asylum seeker and refugee clients must at various times navigate an array of client needs and expectations, political [attacks](https://www.afr.com/news/politics/refugee-lawyers-are-unaustralian-peter-dutton-20170828-gy5cgs) and uncertainties, external timeframes, organisational pressures and professional demands. This applies across the spectrum of activities that lawyers working with and for refugee and asylum seeker clients may undertake: from interviewing clients about their reasons for seeking asylum in Australia, to court appearances where you must articulate your client’s case in relation to established legal principles, to commenting on refugee law and policy in the media or public debates, to public submissions to law reform inquiries, and to undertaking public interest litigation or judicial appeals. 

Key attributes of ethical lawyering with refugee clients may include:

* **Understanding the professional framework of obligations and duties** under both state and Commonwealth laws
* **Conducting yourself in a professional, empathetic and accountable manner** towards your clients
* **Recognising ethical issues** when they arise and having the resources to guide you in your decision-making
* **Being able and willing to communicate** complex legislative frameworks, procedural requirements and difficult legal advice in a simple, clear, accessible and responsive manner to your clients
* **Developing a reflexive practice** of ethical lawyering that understands law – and the role of the refugee lawyer – in their broader political, economic, social and inter-personal contexts
* **Working towards** enacting forms of social justice, responsibility and accountability for refugees and asylum seekers that includes elements of law reform and legal redress for injustices

As student legal volunteers, you will not be providing legal advice to refugee and asylum seeker clients. All your work on refugee casework as part of the Refugee Law Clinic will be supervised by academics and practising solicitors. However, you are still required to have a strong understanding of:

* (i) The legal duties of lawyers working with refugee and asylum seeker clients and the rules relating to professional legal ethics, including the paramount requirement of maintaining client confidentiality.
* (ii) The importance of legal assistance in RSD
* (iii) Ethical lawyering in practices: Key Issues in RSD

## A. Ethical Lawyering in Practice: Key Issues in RSD

Below we list some key ethical issues that inform notions of ethical lawyering working on casework for refugee and asylum seeker clients. The below list is indicative of the ethical considerations that you will need to consider and reflect upon in your clinical project, and we will discuss these further in class. If you have any concerns, please raise these with your supervisor(s).

### (i) Client Confidentiality

As student legal volunteers, it is crucial that you respect the confidentiality of refugee and asylum seeker clients when working on their legal casework. This means treating all personal information about an asylum seeker client found in file documents, court transcripts and other records (even when it is on the public record) as confidential and/or subject to legal privilege. You must not disclose any particular personal information about a client to others outside of the legal team representing the client or working on their particular case. You must also ensure that all documents relating to an asylum seeker client are stored securely and as instructed, as failure to do so may result in confidential information being accessed by others and may constitute unethical conduct.

The [Office of the Migration Agents Registration
Authority](https://www.mara.gov.au/) (OMARA) [Ethical Toolkit explains](https://www.mara.gov.au/media/42408/EthicsToolkit_Navigable_Nov_2011_1_.pdf) (p. 61) the importance of the duty of confidentiality of migration agents as follows:

> Confidentiality lies at the heart of the relationship of trust between a client and their migration agent. It is essential, not only from a legal perspective but also from a broader ethical one, because it provides the basis on which a client can feel safe in divulging all of the necessary information to their adviser, regardless of how personal and potentially damaging it might be. This is what allows the migration agent to do their job and provide full and complete advice and assistance.

Confidentiality is particularly important when working with refugee and asylum seeker clients. Asylum seekers’ claim to asylum is premised upon their fear of returning to their country of origin. This means that they will need to disclose personal information about their circumstances in their country of origins, their present fears in Australia and their past or ongoing actions including in relation to their religious beliefs and practices, ethnicity, sexuality, family relations and/or political opinions. Asylum seekers may be fearful that people in their country of origin including the government, police authorities, or members of their family may have access to any information found within their claims for asylum, and that this might place the safety or well-being of their family or friends at risk in the present or even themselves in the future if they are ever forced to return to their country of origins. For asylum seekers who have experienced torture, physical or sexual violence, their documents may contain information that is particularly sensitive and may be distressing to the asylum seeker if ever made public or disclosed to others outside of their legal representatives with whom they have shared this information. 

Asylum seekers also enjoy the right to privacy under international law (see especially art 17 of the ICCPR). The UNHCR has [stressed](https://www.refworld.org/pdfid/42b9190e4.pdf) that:

> it would be against the spirit of the 1951 Convention to share personal data or any other information relating to asylum seekers with the authorities of the country of origin until a final rejection of the asylum claim.

Because of the high importance of confidentiality in RSD processes, Australian courts and tribunals are required by law to ensure that an applicant for a protection (refugee) visa cannot be identified in documents on the public record. For this reason, published decisions of the Migration and Refugee Division of the AAT are given a case number which is used as the decision name, rather than the applicant’s surname. The applicant’s name is never used in the proceedings. It is also common to find references to specific names, dates or places that may be used to identify the applicant redacted, such as “the applicant travelled to [Country 1]”. 

#### Further resources

* UNHCR, ‘[Advisory opinion on the rules of confidentiality regarding asylum information](https://www.refworld.org/pdfid/42b9190e4.pdf)’ (31 March 2005)

### (ii) Working with Asylum Seeker and Refugee Clients

As student legal volunteers, you are unlikely to be working directly with clients. However, it is important that you have an understanding of practices of ethical lawyering in the context of working with clients that may be highly vulnerable or have experienced past traumas and the systemic barriers that they may encounter as part of the refugee status determination process.

Most asylum seekers have had [traumatic experiences](https://pdfs.semanticscholar.org/f18f/7d2ca3772d0e0a5699e46ce192cfc5a36957.pdf). It is important that lawyers working with asylum seekers and refugees have an understanding of the literature on trauma and are alert and sensitive to the potential that their clients may be impacted by past trauma. In addition, the refugee status determination process is usually exceptionally stressful and may cause refugee and asylum seeker clients to experience further trauma or distress. Ethical lawyering requires lawyers working with asylum seekers and refugees to actively try to minimise where possible the stress experienced by their asylum seeker client during the process, while also recognising that it is important for clients to articulate and present their claims to asylum in their fullest, frankest sense. This may mean inviting a client to elaborate on a past experience of trauma in support of their claims to asylum or making a client aware that a decision-making may inquire into past experiences of persecution and harm.

Yet while certain information about past experiences of persecution may strongly support a client’s protection claim, they can also trigger trauma responses. As [Mary Anne Kenny and Lucy Fiske have demonstrated](http://www.austlii.edu.au/au/journals/AJHR/2004/21.html), these trauma responses can ‘significantly impact on the asylum seeker’s ability to disclose information and to respond to questions in an interview’. This may mean that an asylum seeker is unable to present the traumatic experiences in a logical and coherent manner, which may be detrimental to the assessment of their credibility. Research has shown that refugee status determination processes routinely fail to adequately account for the effect of trauma on asylum seekers. RSD is premised upon a false assumption that all refugees should be able to remember and narrate their experiences in a clear, coherent and comprehensive manner, and decision-makers are encouraged to assume that any discrepancies in oral testimony or written evidence are signs that the asylum seeker’s claims are fabricated or embellished. These questions of credibility are critical to the determination of refugee status.

In addition to the past experience of trauma, the process of refugee status determination presents challenges and stressful situations for both asylum seekers and lawyers working with asylum seekers. While a lawyer working with refugee and asylum seeker clients may be unable to change the structural and procedural demands imposed by the refugee status determination process, there are practical, concrete steps that they can take that may make their interactions with their clients less distressing and improve their client’s ability to engage with the RSD process. These are issues to consider even though your clinical work may not involve direct client contact and [include](http://www.austlii.edu.au/au/journals/AJHR/2004/21.html):

* **During an initial meeting with an asylum seeker client:**
  * Establish client trust and rapport by having a well-thought and prepared and organised meeting.
  * Explain the purpose of meeting and types of questions that will be asked.
  * Acknowledge that the interview might be upsetting.
  * Take the time to explain the legal requirements and process with your client.

* **In preparing for the department interview:**
  * The representative does not generally intervene during the interview.
  * Before the interview meet with your client to discuss the purpose of interview, refugee status criteria, and their written statement.
  * Reinforce the positive elements of the interview with the client after the interview.

* **In the case that asylum seeker’s claim is refused:**
  * Asylum seekers require intensive support immediately after refusal.
  * Asylum seekers may see the refusal of their application as a personal rejection and as disbelief in their story. 
  * Asylum seekers need advice on appeal and implications of the refusal.

#### Further resources

* Mary Anne Kenny and Lucy Fiske, ‘[“Marriage of Convenience” or a “Match Made in Heaven”: Lawyers and Social Workers Working with Asylum Seekers](http://www.austlii.edu.au/au/journals/AJHR/2004/21.html)’ (2004) 10(2) Australian Journal of Human Rights 21
* Australian Refugee Health Practice Guide, [Management of psychological effects of torture or other traumatic events](http://refugeehealthguide.org.au/psychological-effects-of-torture-trauma/).
* Jane Herlihy and Stuart W Turner, ‘[The Psychology of Seeking Protection](https://pdfs.semanticscholar.org/f18f/7d2ca3772d0e0a5699e46ce192cfc5a36957.pdf)’ (2009) 21 International Journal of Refugee Law 171.

### (iii) Working with Interpreters in Refugee Casework

> ‘Asylum seekers are in a difficult position. They have been forced to leave their home countries and are seeking refuge in an unfamiliar country.  Experiences at home or on their journey may have left them sad, grieving or traumatised. They often do not understand the format, structure and communicative norms of asylum procedure. For them, the interpreter is often the first person with whom they can communicate in their native language, or a language they understand, and who can explain the procedure.’[^1]

It is common to work with and rely upon interpreters when working with refugee and asylum seeker clients. The asylum procedure is usually conducted in the host country’s official languages(s), and protocols are written in this language.[^2] Where applicants for asylum lack the degree of proficiency in this language needed to enable them to take part in the hearing, interpreters can fulfil a range of functions:

* Interpreters allow asylum seekers to more fully understand the refugee status determination process
* Interpreters facilitate asylum seekers’ claims and testimony into the refugee status determination process.
* Interpreters can ensure that culturally-specific terminology or use of words are translated in an accurate way and placed in their proper context.

Given the significant consequences of failed asylum applications, such as deportation, interpreters carry a great burden of responsibility. Poor Interpreting can put asylum seekers at risk of being found untrustworthy or lacking in credibility. A lot hangs on the testimony of asylum seekers: it is often one of the main ways that an asylum seeker can demonstrate that they have a well-founded fear of persecution and that they cannot return to their home country. It is therefore crucial that asylum seeker claims and statements are translated in an accurate, exact and careful manner.

In Australia at the request of the asylum applicant an interpreter is provided for Departmental interviews, and for administrative review hearings, and appeal hearings free of charge. Asylum seekers need only to provide credible assertions relevant to their claim for asylum and do not have to provide proof.[^3] However, this service does not extend to lawyers providing immigration advice and assistance to asylum seekers. Community legal centres assisting some asylum seekers must bear the cost of an interpreter or rely upon volunteer interpreters, as the interpreters are essential for most asylum interviews.
 
As student legal volunteers working on refugee casework, it is unlikely that you will need to directly work with interpreters. That said, you may need to be attuned to the issues that can arise when relying on statements, interview transcripts or other documents that have been affected by interpretation. For example, if there are inconsistencies between documents, you may need to consider whether these inconsistencies are the product of interpretation services or something else. Also, you may need to raise legal arguments concerning the quality and effect of interpretation on the refugee status determination procedure. The Australian courts have considered what standard of interpretation is required, holding that ‘adequate rather than perfect’ level of interpreting is sufficient for facilitating procedural fairness.[^4] In Perera, Kenny J’s held that: 

> The function of an interpreter in the Tribunal (as in a court) is to place the non-English speaker as nearly as possible in the same position as an English speaker. In other words, an interpreter serves to remove any barriers which prevent or impede understanding or communication. An interpreter provides the means for communication between the applicant, the Tribunal and other participants in the Tribunal hearing, in cases where the applicant’s own linguistic capacities are not, on their own, sufficient to that end.[^5] 

In the context of working with interpreters, ethical lawyering however also entails ensuring that your client is comfortable with the interpreter being used and is able to communicate with them with the necessary degree of ease and openness (including being aware of issues of gender, dialect, ethnicity, level of professionalism or other factors).

The UNHCR has developed a [Handbook for Interpreters in Asylum Procedures (2017)](https://www.refworld.org/docid/59c8b3be4.html) that explains the role of interpreters in refugee status determination processes and sets out some key interview techniques.

#### Further resources:

* Migration and Refugee Division, AAT, [Guidelines for Interpreters](https://www.aat.gov.au/AAT/media/AAT/Files/MRD%20documents/Legislation%20Policies%20Guidelines/Guidelines-for-Interpreters.pdf) (July 2015).
* Matthew Groves, ‘[Interpreters and Fairness in Administrative Hearings](https://law.unimelb.edu.au/__data/assets/pdf_file/0010/2369584/03-Groves-402-Post-Press.pdf)’ (2016) 40 Melbourne University Law Review 506, esp 513-517. 
* Muneer Ahmad, ‘[Interpreting Communities: Lawyering across Language Difference](https://digitalcommons.law.yale.edu/cgi/viewcontent.cgi?referer=https://www.google.com/&httpsredir=1&article=6260&context=fss_papers)’ (2007) 54 UCLA Law Review 999.

## B. Legal Duties of Lawyers working with Refugee and Asylum Seeker Clients

Lawyers working with refugee and asylum seeker clients have duties to both their clients and to the Court. These duties have been the subject of extensive elaboration and scrutiny by the courts, academics, legal professional bodies and lawyers themselves. Suffice to say, there are a wealth of legal instruments, cases, textbooks and other documents that unpack the precise nature of lawyers’ professional duties to the Courts, their clients and the profession. The below section summarises the key legal instruments and duties that you need to be mindful of as student legal volunteers working on refugee casework. As student legal volunteers, although you will not provide advice you will be bound by a volunteer agreement. You will also be closely supervised in your casework by academics and practising solicitors, the latter will be subject to the below duties and professional responsibilities. 

### (i) NSW Legislation

In NSW, the relevant legislation covering the professional rules of legal professionals is the [Legal Profession Uniform Law Australian Solicitors’ Conduct Rules 2015](https://www.legislation.nsw.gov.au/#/view/regulation/2015/244) (NSW). This legislation covers the fundamental duties of solicitors and stresses that solicitors’ paramount duty is to the court and the administration of justice (see s 3). It also stipulates other fundamental ethical duties of solicitors, including:

* to act in the best interest of a client in any matter (s 4.1.1)
* to be honest and courteous in all dealings in the course of legal practice (s 4.1.2)
* to deliver legal services competently, diligently and as promptly as reasonably possible (s 4.1.3)
* to avoid any compromise to their integrity and professional independence (s 4.1.4), and
* to comply with the rules found within the [Legal Profession Uniform Law Australian Solicitors’ Conduct Rules 2015](https://www.legislation.nsw.gov.au/#/view/regulation/2015/244) and the law (s 4.1.5). 

Other sections of the [Legal Profession Uniform Law Australian Solicitors’ Conduct Rules 2015](https://www.legislation.nsw.gov.au/#/view/regulation/2015/244) deal with the mandated conduct of legal professionals in their relations with clients (including the communicating advice, taking instructions, respecting client confidentiality and client documents), advocacy and litigation (including conduct in court, handing evidence and communicating with opponents), and law practice management. To act ethically as a lawyer working with refugee and asylum seeker clients, solicitors need to have a solid understanding of your professional duties under these rules and how these apply in practice.  Solicitors are also [required](https://www.legislation.nsw.gov.au/#/view/regulation/2015/242/full) to complete regular training in ethical responsibility to meet their ongoing Continuing Professional Development (CPD) requirements for maintaining their practicing certificates.  
Disciplinary action may be taken against solicitors who fail to comply with their professional duties under the [Legal Profession Uniform Law](https://www.legislation.nsw.gov.au/#/view/act/2014/16a/chap5/part5.4) (NSW). Section 296 of the Act defines ‘unsatisfactory professional conduct’ as including:

> conduct of a lawyer occurring in connection with the practice of law that falls short of the standard of competence and diligence that a member of the public is entitled to expect of a reasonably competent lawyer.

This can include any conduct that may lead to the finding that a person is ‘not a fit and proper person to engage in legal practice’ (s 197(1)(b)).

The [Law Society of NSW](https://www.lawsociety.com.au/practising-law-in-NSW/ethics-and-compliance/ethics/statement-of-ethics) has adopted a Statement of Ethics that elaborates on a lawyer’s professional role as an officer of the Court. The Statement commits lawyers to enact the following ethical and legal commitments:

* To primarily serve the interests of justice.
* To act competently and diligently in the service of clients.
* To advance clients' interests above their own.
* To act confidentially and in the protection of all client information.
* To act together for the mutual benefit of the profession.
* To avoid any conflict of interest and duties.
* To observe strictly our duty to the court to ensure the proper and efficient administration of justice.
* To seek to maintain the highest standards of integrity, honesty and fairness in all dealings.
* To charge fairly for work.

The Law Society of NSW has an [Ethics
Unit](https://www.lawsociety.com.au/practising-law-in-NSW/ethics-and-compliance/ethics/about) that can offer independent, experienced [guidance on ethical issues](https://www.lawsociety.com.au/practising-law-in-NSW/ethics-and-compliance/ethics/committee-guidance) and runs [regular education sessions for legal practitioners](https://www.lawsociety.com.au/practising-law-in-NSW/ethics-and-compliance/ethics/education). 

Further resources on ethical conduct, including a helpful [FAQ Guide](https://www.lawsociety.com.au/sites/default/files/2018-03/FAQs.pdf), can be found on the [Law Society of NSW website](https://www.lawsociety.com.au/practising-law-in-NSW/ethics-and-compliance/ethics/education).

### (ii) Commonwealth Legislation

Lawyers providing migration advice and assistance to asylum seeker and refugee clients in Australia are also subject to the professional requirements of the [Migration Act 1958](https://www.legislation.gov.au/Series/C1958A00062) (Cth). This Commonwealth legislation establishes a regulatory framework that governs the provision of immigration assistance in Australia. It mandates, subject to some limited exceptions, that only registered migration agents are permitted to provide immigration assistance in Australia (see Part 3: Migration agents and immigration assistance, esp s 280). Section 276 of the Migration Act defines ‘immigration assistance’ as when a person, for example ‘uses, or purports to use, knowledge of, or experience in, migration procedure to assist a visa applicant or cancellation review applicant by:

* (a)  preparing, or helping to prepare, the visa application or cancellation review application; or
* (b)  advising the visa applicant or cancellation review applicant about the visa application or cancellation review application; or
* (c)  preparing for proceedings before a court or review authority in relation to the visa application or cancellation review application; or
* (d)  representing the visa applicant or cancellation review applicant in proceedings before a court or review authority in relation to the visa application or cancellation review application.

Immigration assistance also covers acts in relation to certain request to the Minister (see s 276(2A)). Under s 276(3), a person does not give immigration assistance if they ‘merely’:

* (a)  do clerical work to prepare (or help prepare) an application or other document; or
* (b)  provide translation or interpretation services to help prepare an application or other document; or
* (c)  advise another person that the other person must apply for a visa; or
* (d)  pass on to another person information produced by a third person, without giving substantial comment on or explanation of the information.

The circumstances in which a lawyer gives ‘immigration legal assistance’ are set out under s 277.

The Migration Act 1958 (Cth) also establishes the [Office of the Migration Agents Registration Authority](https://www.mara.gov.au/) (OMARA) that is responsible for administering the registration of migration agents and for promoting and monitoring the ‘integrity’ and ‘quality’ of immigration assistance under the Act. This can include hearing [complaints](https://www.mara.gov.au/using-an-agent/resolving-disputes-with-your-agent/make-a-complaint-about-an-agent/) from clients against their migration agents and undertaking [disciplinary action](https://www.mara.gov.au/using-an-agent/resolving-disputes-with-your-agent/complaint-outcomes/), including the cancellation or suspension of a migration agent’s registration.

OMARA has developed a [Code of Conduct](https://www.mara.gov.au/becoming-an-agent/professional-standards-and-obligations/code-of-conduct/) that binds registered migration agents to certain ethical standards and principles. These include acting in accordance with the law and the ‘legitimate interests of his or her client’ (s 2.1(a)) and dealing with their client ‘competently, diligently and fairly’ (s 2.1(a)).

This means that lawyers providing immigration assistance to asylum seekers and refugee clients in Australia must also be registered migration agents under the Migration Act 1958 (Cth). In 2011, over 26 per cent of registered migration agents were also Australian lawyers. OMARA refers to such lawyers as ‘lawyer migration agents’ and [notes that](https://www.mara.gov.au/media/42408/EthicsToolkit_Navigable_Nov_2011_1_.pdf):

> When a lawyer migration agent provides a service that involves the giving of immigration assistance the lawyer is also acting as a registered migration agent and must take care to ensure that his or her conduct does not breach obligations under either the Legal Profession Acts or the standards of professional conduct including the Rules or Code of Conduct. **In other words, they will have two sets of rules that apply to their conduct and these will not always be consistent with each other**.

For example, while migration agents are required to act lawfully, they are not officers of the court and owe no specific duty to the administration of justice. Lawyer migration agents are subject to a broader notion of lawyer-client privilege than the duty of confidentiality that applies to migration agents.

[OMARA has published an Ethical Toolkit](https://www.mara.gov.au/media/42408/EthicsToolkit_Navigable_Nov_2011_1_.pdf) to provide practical advice to all migration agent about professional ethics and working with a diverse range of clients to provide immigration advice.

## C. The Importance of Legal Assistance in RSD

Ensuring that asylum seekers have access to legal assistance and representation in refugee status determination is crucial. The refugee status determination process can be technical, complicated and subject to specific procedural requirements. The stakes are also high: submitting an incomplete application for asylum, or relying upon an application that fails to meaningfully substantiate a person’s claim for asylum, can result in a negative outcome and eventual deportation back to a place where an asylum seeker may fear persecution and experience substantial and ongoing harm. 

International law provides that all people ‘shall be equal before the courts and tribunals’ (see [article 14 of the International Covenant on Civil and Political Rights](https://treaties.un.org/doc/publication/unts/volume%20999/volume-999-i-14668-english.pdf) (ICCPR)). The UN Human Rights Council has [held](https://www.refworld.org/docid/478b2b2f2.html) that this obliges states to ensure that individuals have ‘equal access and equality of arms, and ensures that the parties to the proceedings in question are treated without any discrimination’. 

International law expressly recognises the relationship between the right to equality before the law and the provision of legal representation and assistance, particularly in the context of a criminal trial. As [noted by the UN Human Rights Council](https://www.refworld.org/docid/478b2b2f2.html), this means that the ‘availability or absence of legal assistance often determines whether or not a person can access the relevant proceedings or participate in them in a meaningful way’. States are thus encouraged – and at times obliged – to provide free legal assistance for individuals in non-criminal trials without ‘sufficient means to pay for it’ themselves. This free provision of legal assistance in Australia is particularly important in the case of undocumented migrants and asylum seekers, who may have a limited knowledge of English or the Australian legal system and few financial resources.

The [UNHCR has emphasised](https://www.refworld.org/docid/432ae9204.html) the importance of access to free legal assistance in the case of asylum seekers:

> Asylum-seekers are often unable to articulate the elements relevant to an asylum claim without the assistance of a qualified counselor because they are not familiar with the precise grounds for the recognition of refugee status and the legal system of a foreign country. Quality legal assistance and representation is, moreover, in the interest of States, as it can help to ensure that international protection needs are properly identified. The efficiency of first instance procedures is thereby improved. **In UNHCR’s view, the right to legal assistance and representation is an essential safeguard, especially in complex asylum procedures. It is also important to guarantee free legal assistance and representation in first instance procedures and against negative decisions**.[^6] 

[Since 2014](https://www.sbs.com.au/news/government-cuts-legal-aid-for-asylum-seekers), asylum seekers who arrived in Australia without a valid visa are no longer eligible for free legal assistance previously provided under [Immigration Advice and Application Assistance Scheme](https://immi.homeaffairs.gov.au/visas/getting-a-visa/visa-listing/protection-866/iaaas) (IAAAS). Currently, only a limited number of ‘highly vulnerable’ asylum seekers who arrived by boat are eligible for funded immigration advice and application through the [Primary Application Information Service](https://www.asrc.org.au/wp-content/uploads/2013/07/PAIS-Fact-Sheet-May-2016.pdf) (PAIS).

This lack of government-funded legal assistance has meant that the majority of asylum seekers have to rely upon the free legal services offered by community legal centres. These legal centres receive no funding from the Commonwealth Government for assisting asylum seekers, except for the small portion of asylum seekers funded under the PAIS and IAAAS. Instead, these centres operate largely through philanthropic donations and mobilising volunteers, including law students and lawyers offering pro bono services. This drastic reduction in Federal Government funding has placed exceptional pressure on community legal centres providing pro bono legal service to asylum seekers (see, for example, [RACS’ statement on funding](https://www.racs.org.au/funding/)).

As student volunteers working on refugee casework for a community legal centre, it is important to understand the significance of your work and the legal and political climate within which you are working. 

### Further resources

* UNHCR, [Asylum Processes (Fair and Efficient Asylum Procedures)](https://www.refworld.org/docid/3b36f2fca.html), Doc No EC/GC/01/12 (31 May 2001).
* Kaldor Centre for International Refugee Law, UNSW, ‘[Do people seeking asylum receive legal assistance?](https://www.kaldorcentre.unsw.edu.au/publication/legal-assistance-asylum-seekers-0)’ (Factsheet, October 2018).
* Sara Dehm and Anthea Vogl, [An Unfair and Dangerous Process: A Legal Analysis of the Ministerial Deadline to Apply for Asylum and Use of Executive Power in the Legacy Caseload](https://academicsforrefugees.files.wordpress.com/2017/10/an-unfair-and-dangerous-process-final.pdf) (Academics for Refugees Report, October 2017).



[^1]: Handbook for Interpreters in Asylum Procedures Page 55.

[^2]: Ibid page 41.

[^3]: Handbook for Interpreters in Asylum Procedures Page 40.

[^4]: Ibid.

[^5]: Ibid 511, Quote from Perera v Minister for Immigration and
Multicultural Affairs (1999) 92 FCR 6, 18[24].

[^6]: UNHCR, ‘Fair and Efficient Asylum Procedures: A Non Exhaustive Overview of Applicable International Standards’ (2 September 2005) 3 (emphasis added).
