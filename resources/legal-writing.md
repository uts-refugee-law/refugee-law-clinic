---
postTitle: Legal writing, a quick guide
description:
  This guide to formal legal writing explains what is expected of you when drafting legal documents and contains resources to help you develop and improve your legal writing skills.
date: 2019-03-07
---

Good legal writing is an essential and useful tool for legal practitioners. Legal writing is a process of integrating legal theory and authority into legal documents or submissions. As Sherri Lee Keene [emphasises](https://digitalcommons.law.umaryland.edu/cgi/viewcontent.cgi?httpsredir=1&article=2321&context=fac_pubs), good legal writing involves making important professional and strategic decisions as well as core skills of clear expression and strong legal drafting.

## Contents

* [Clear, concise and credible Legal writing](#clear%2C-concise-and-credible-legal-writing)
* [Legal Writing in Practice](#legal-writing-in-practice)
  * [‘Plain Language’ Writing: What is it?](#%E2%80%98plain-language%E2%80%99-writing%3A-what-is-it%3F)
  * [Tips for Good Legal Drafting](#tips-for-good-legal-drafting)
* [Types of Legal Writing](#types-of-legal-writing)
* [Further Reading](#further-reading)

## Clear, concise and credible Legal writing

Good legal writing should be clear and succinct, and integrate legal authority into a clear legal argument.  Clear and concise legal writing delivers a ‘[direct and focused](https://digitalcommons.law.umaryland.edu/cgi/viewcontent.cgi?httpsredir=1&article=2321&context=fac_pubs)’ message without neglecting or overstating a legal argument. 

Clarity requires more than logical structure and readability. Clear legal writing should address each of the legal issues, and carefully interpret and apply relevant authority in order to argue for a particular finding or outcome. Good legal writing focusses on relevant information and authorities, and involves making difficult decisions about what should be included or excluded, as well as what should be prioritised. Generally, submissions must address authorities that support or conflict with the case or argument that you are advancing.  

Good legal writing moves beyond mere description or summary. It is a manifestation of the writer’s knowledge of the law as applied to the case at hand. As Keene [highlights](https://digitalcommons.law.umaryland.edu/cgi/viewcontent.cgi?httpsredir=1&article=2321&context=fac_pubs), it involves professional judgments that are informed by professional norms, conventions and ethical rules. Credible legal writing should not mislead the reader and will reflect one’s duty to both the client and to the Court or decision-making body.

As you consider the more practical skills and aspects of legal writing, when undertaking your clinical work it is important to keep in mind law’s relationship with language, and in particular the way law and legal institutions often uses language in order to exercise power. As Conley and O’Barr note, for many people, law’s power manifests through language and less in the language of superior court decisions and formal legislation ‘than in the details of legal practice, in the thousands of mini-dramas reenacted every day in lawyers’ offices, police stations and courthouses around the country.’ 

## Legal Writing in Practice

### ‘Plain Language’ Writing: What is it?

Legal writing should be drafted in ‘plain language’ or what was formerly referred to as ‘plain English.’ As Terry Hutchinson writes, ‘the plain legal language movement has gained momentum as a reaction to criticisms that legal language is inaccessible.’ Fundamentally ‘plain English’ as defined by the Oxford English Dictionary is ‘clear and unambiguous language, without the use of technical or difficult terms.’

Hutchinson further explains that ‘while recognising that a legal document must be technically correct and accurate, the principle is that is remains intelligible to its primary audience’.  The word, structure and tone of a document are all crucial to achieving plain legal language, and a core principle is the either the removal of unnecessarily technical terms or explaining these terms (such as Latin phrases) where they need to be included. 

A preference for plain language is now generally accepted in most aspects of the legal profession and legal drafting, even if many documents, judgments or statutes fall short of this expectation! As Peter Butt [outlines](http://www.lawfoundation.net.au/ljf/app/&id=/2FD34F71BE2A0155CA25714C001739DA) there is support for plain language in many jurisdictions. 

* ‘Most large law firms now have plain language “units”, whose function is systematically to change [or ensure] the firm’s standard precedents to a plain language style.
* Law Societies and Bar Associations in a number of jurisdictions now actively promote plain language. …
* Law Reform Commissions in a number of countries have issued reports urging the adoption of a plain language drafting style. …
* Parliamentary drafters in most Australian jurisdictions now consciously adopt a plain language style.’

### Tips for Good Legal Drafting

Like all writing, your legal writing will be improved through an iterative process of drafting and redrafting and very careful editing. As well, rules prohibiting plagiarism and requiring careful citation of all sources and authorities also govern legal drafting.

For a good checklist of principles to guide and assess your approach to legal writing see: [Catriona Cook et al, Laying Down the Law (2012, 8th  ed, LexisNexis Butterworths)](https://drr.lib.uts.edu.au/43248/70102_CookWriting.pdf), 477-496. Cook et al provide guidance for your writing on the following topics and principles:

* Precision
* Simplicity
* Formality
* Active voice
* Legal terminology and jargon
* Avoidance of discriminatory language
* Use of supporting authority and originality

The Hon. Michael Kirby, a long-time advocate of plain legal language assembled a helpful set of principles, to assist you in ensuring your legal writing is always in plain language. He names these rules, [Clarity’s Ten Commandments](https://www.michaelkirby.com.au/images/stories/speeches/2013/2651%20-%20SPEECH%20-%20LAW%20SOCIETY%20OF%20NEW%20SOUTH%20WALES%20LAW%20SOCIETY%20JOUNAL%20-%20FEBRUARY%202013%20-%20HOW%20I%20LEARNED%20TO%20DROP%20LATIN%20AND%20LOVE%20PLAIN%20LEGAL%20LANGUAGE.pdf) and they are as follows:

1. Begin complex statements of fact and law with a summary (the issue, crucial facts and answer) to let readers know where they are going. 
2. Break up long sections and paragraphs. Group related materials together, and order the parts in a sequence that will appear logical to readers. 
3. Pay careful attention to layout (type size, typeface, whitespace, indenting, and the like). And use plenty of headings and subheadings to identify the progression of ideas.
4. When conveying detailed information, regularly use vertical lists. Where appropriate, number the items as these Commandments are numbered, for later reference. 
5. Prefer short and medium-length sentences. As a guideline, keep the average sentence length to about 20 words. 
6. For continuity between sentences, put old information - some word or idea from the previous sentence - at or towards the beginning of the sentence. ('No more legalese. It has been ridiculed long enough.') And end sentences forcefully by putting your strongest point, your most important information, at the end. 
7. Prefer the active voice to the passive. (You should know the difference.) Use the passive voice selectively, for example, if the agent is unknown or not understood. Or if you want to focus attention on the object of the action instead of the agent-typically, so that old information is upfront. 
8. Prefer verbs to noun phrases ('consider', not 'give consideration to').
9. Prefer the familiar words - usually the shorter ones with Germanic roots - that are simple and direct and human. Avoid old potboilers ('whereas', 'hereinunder', 'cognisant', 'requisite', 'pursuant to', and all the rest). In drafting, banish 'shall' wherever possible.
10. Avoid unnecessary detail and words. Take special aim at multi-word prepositions ('prior to' , 'with regard to' , 'in connection with') and unnecessary prepositional phrases. (Delete 'the duty of the landlord' and substitute 'the landlord's duty'; and 'order of the court', and replace it by 'a court order').

## Types of Legal Writing

Working in legal clinic may involve a range of different kinds of writing. It may require you to complete a visa application forms, a Freedom of Information request, or an application for judicial review. It may also require you to prepare written submissions, a statutory declaration or a draft letter of advice for a client, or correspondence with someone other than the client.

For a detailed description of the following key legal documents and practical drafting points see: Nichola Corbett-Jarvis and Brendan Grigg, Effective Legal Writing: A Practical Guide (LexisNexis Butterworths, 2nd ed, 2017) Chapter 6, Profession-Orientated Writing.

* Written submission
* Affidavits
* Provision of Advice, communication with the client
* Correspondence with someone other than the client
* Brief to counsel

## Further Reading

* Michael Kirby, ‘[How I Learned to Drop Latin and Love Plain English](https://www.michaelkirby.com.au/images/stories/speeches/2013/2651%20-%20SPEECH%20-%20LAW%20SOCIETY%20OF%20NEW%20SOUTH%20WALES%20LAW%20SOCIETY%20JOUNAL%20-%20FEBRUARY%202013%20-%20HOW%20I%20LEARNED%20TO%20DROP%20LATIN%20AND%20LOVE%20PLAIN%20LEGAL%20LANGUAGE.pdf)’ (2013) 53 Law Society Journal (online)
* Samuel Murumba, ‘[Good Legal Writing: A Guide for the Perplexed](https://drr.lib.uts.edu.au/23288/)’ (1991) 17 Monash University Law Review 93. 
* Pamela Samuelson, ‘[Good Legal Writing: Of Orwell and Window Panes](https://drr.lib.uts.edu.au/43905/)’ (1984-85) 46 University of Pittsburgh Law Review 149. 
