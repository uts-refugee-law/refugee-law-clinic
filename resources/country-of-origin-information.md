---
postTitle: Country of Origin Information
description:
  This guide explains the role and use of Country of Origin Information in applications for refugee status, and where to find authoritative country information sources.
date: 2019-02-24
---

## Contents

* [What is country information and why is it necessary?](#what-is-country-information-and-why-is-it-necessary%3F)
* [Not all Sources are Created Equal: how to identify and find authoritative sources](#not-all-sources-are-created-equal%3A-how-to-identify-and-find-authoritative-sources)
* [Key Sites and Sources for Country Information](#key-sites-and-sources-for-country-information)
  * [1. European Country of Origin Information Network’s Compilation of Country Information](#1.-european-country-of-origin-information-network%E2%80%99s-compilation-of-country-information)
  * [2. Refworld](#2.-refworld)
  * [Other sources](#other-sources)
* [Further Reading](#further-reading)

## What is country information and why is it necessary?

When preparing written submissions in support of a refugee application, research on the situation in the applicant’s home country and the use of what is called ‘**country information**’ or ‘**country of origin information**’ is essential.  As UNHCR’s resource collection, [Refworld](https://www.refworld.org/category,COI,,,,,0.html#bbtype) explains:

> Country information is vital for the refugee status determination procedure, as it assists the decision maker on issues of credibility and the objective situation in a country of origin in relation to an individual's claim. 

Generally, country information is information that addresses the political, human rights or social and cultural situation in the applicant’s country of origin. Country information may, for example, address the effects of laws, governments or non-state actors, or of recent events, wars or other political turmoil for specific groups or peoples living in a country or in a region within a country. 

Careful research to find and then incorporate of country information into your submissions will be critical to showing that the applicant’s claims are credible and well-founded, and that the claim can be supported by reliable information about the situation of people or groups in a similar position to the applicant in her/his country of origin.

Country information might, for example, explain the situation for Hazaras in Afghanistan after the fall of the Taliban, or the situation for members of the Chin minority living in Burma. When compiling a claim, it is essential to support it with information that shows that applicant’s claims are reinforced by independent information about the situation in the country of origin. Where independent country information is not available or conflicts with the applicant’s claim, it may be necessary to address this, and explain why the applicant’s experiences are unique or undocumented, but nonetheless credible and well-founded. 

## Not all Sources are Created Equal: how to identify and find authoritative sources

One of the challenges of country information is finding information that deals as directly as possible with the details of an applicant’s claim and making sure that information comes from a reputable source. 

As the Refworld website [describes](https://www.refworld.org/category,COI,,,,,0.html#bbtype) ‘country information collection consists of a wide variety of sources selected from UN, NGOs, Governments, International Organisations and others monitoring and reporting on country situations.’

So, country information may be produced by a human rights organisation and address a particular conflict or policy, or may be a newspaper article showing that attacks on a particular minority group have increased in recent times. It may also be a report on a situation in a particular country produced by another government, such as the Australian Department of Foreign Affairs and Trade (DFAT).

Helpfully, there are a number of clearly authoritative sources, and many of these have already been compiled for refugee advocates, refugees, lawyers and decision-makers by the *UNHCR* and the *European Country of Origin Information Network* [here](https://www.ecoi.net/). You can find a list of the **key sources compiled for you** below. 

If you able to find information from key sources, this is good and important place to start but you may need to widen your search. At times, you may need to look beyond these sources to more current reports, news reporting, advocacy groups, academic articles or other sites. When judging a source of country information, the following factors will be relevant and important to consider:

* **Reputation, reliability and independence of the source organisation or author**: who is the author and what is their motivation? Are they connected to a particular group? How does the source of the document affect its reliability and perceptions of its authority? 

* **Quality of the information in the source**: does the source cite sources where appropriate? What is the tone, purpose or aim of the source? Is the document prepared with an overtly political motivation and does it retain authority if so?

* **Timeliness**: does the source cover the time of your applicant’s claim? Is it too old? Is it too recent?

* **Specificity of the source**: is it too general? Too specific? Does it address a country as a whole? Would regional or city-specific information be more effective. 

* **Reception of source**:  has this source been used in previous refugee status determination decisions? If so, how has it been used and what weigh has been given to it by RSD decision-makers? If not, what kinds of sources have been preferred or more readily referred to by decision-makers similar claims from the same country of origin?

## Key Sites and Sources for Country Information

Start with these sites when searching for information. 

### 1. European Country of Origin Information Network’s Compilation of Country Information

[European Country of Origin Information Network’s (ECOI) Compilation of Country Information](https://www.ecoi.net/) ‘covers more than 160 sources on a regular basis. [ecoi.net](https://www.ecoi.net/) preferably uses reliable information sources or sources with a high reputation, such as the United Nations, international non-governmental organisations, news and media services or government departments dealing with asylum and refugee issues. However, [ecoi.net](https://www.ecoi.net/) will also include less prestigious sources if they offer an added information value.’

ECOI usefully [lists all the sources it covers](https://www.ecoi.net/en/about/our-sources/) and includes information from refugee-receiving states’ own ‘country of origin information services.’ These bodies are often located within refugee decision-making bodies and collect country information that is used to judge and determine refugee claims.  ECOI also searches communiques or ‘query responses’, where Governments have sought to answer questions about specific conditions in a particular country. In Australia these are often in the form of DFAT cables or reports. It is helpful to remember that these are Government sources, and so they are not formally independent, however they may be highly regarded by decision-makers as they are part of the Government’s own research and reporting mechanisms.  

### 2. Refworld

[Refworld](https://www.refworld.org/) is the UNHCR’s online repository of protection and research information, to assist those involved in refugee status determination decisions and processes. See especially Refworld’s Country Information tab, though note that much of this information is now being collated on the ECOI site.

### Other sources

The following list includes a number of key sources of country information. It will be useful to become familiar with these organisations and their work, in addition the important compilation of these sources on the European Country of Origin Information Network website.

* [Human Rights Watch](https://www.hrw.org/world-report/2019) (HRW), including the annual [HRW World Report](https://www.hrw.org/world-report/2019) and the HRW Country Reports.

* [Amnesty International](https://www.amnesty.org/en/), including the annual [Amnesty International Report](https://www.amnesty.org/en/documents/pol10/6700/2018/en/) and country reports.

* [Office of the United Nations High Commissioner for Human Rights](http://www.ohchr.org) (OHCHR): 

* [US Department of State Human Rights Reports](https://www.state.gov/j/drl/rls/hrrpt/) The annual Country Reports on Human Rights Practices ‘cover internationally recognized individual, civil, political, and worker rights, as set forth in the **Universal Declaration of Human Rights** and other international agreements. The U.S. Department of State submits reports on all countries receiving assistance and all United Nations member states’. See also the [Department of State’s International Religious Freedom Reports](https://www.state.gov/j/drl/rls/irf/).

* [European Asylum Support Office](https://www.easo.europa.eu/information-analysis) Country Information Reports and Guidance

* [Internal Displacement Monitoring Centre](http://www.internal-displacement.org/) – run by the Norwegian Refugee Council it aims to ‘provide high-quality data, analysis and expertise on internal displacement with the aim of informing policy and operational decisions that can reduce the risk of future displacement and improve the lives of internally displaced persons (IDPs) worldwide’

* [Freedom House](http://www.freedomhouse.org/reports) advocates for democratic change around the world. It produces analysis and publishes periodic country reports on political rights and civil liberties in various countries and regions with headquarters in Washington, DC. 

* For international media and press sources see: [Associated Press (AP)](http://www.ap.org); [Reuters](http://www.reuters.com); [Agence France-Presse (AFP)](http://www.afp.fr) 

For a very comprehensive list of Country Information, including UN sources and UN Treaty reports and media sources, see [Australian Red Cross, *Researching Country of Origin Information: Training Manual* (2013)](https://www.coi-training.net/site/assets/files/1021/researching-country-of-origin-information-2013-edition-accord-coi-training-manual.pdf) at Appendix B. 

## Further Reading

* The [Australian Red Cross, *Researching Country of Origin Information: Training Manual* (2013)](https://www.coi-training.net/site/assets/files/1021/researching-country-of-origin-information-2013-edition-accord-coi-training-manual.pdf) provides an excellent overview of what country information is, how to use it and where to find it. It also includes draft ‘research strategies’ and checklists to help you assess the value and reliability of country information sources. 
